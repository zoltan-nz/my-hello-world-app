# My Hello World App

Install Python on Mac: https://github.com/pyenv/pyenv#homebrew-on-macos

```
pyenv install 3.7.4
pyenv global 3.7.4
pyenv versions
pip install pipenv
```

Pipenv run options:

| Pipenv task               | Description                                                                          |
| ------------------------- | ------------------------------------------------------------------------------------ |
| `pipenv run setup`        | Setup local pip environment. Run it first.                                           |
| `pipenv run clean`        | Clean up temp files.                                                                 |
| `pipenv run lock-req`     | Update `requirements.txt`. Run it if you added or upgraded some packages in Pipfile. |
| `pipenv run server-prod`  | Run your Flask app in production.                                                    |
| `pipenv run server-watch` | Run your Flask app in dev mode with watch task.                                      |
| `pipenv run lint`         | Run pylint.                                                                          |
| `pipenv run format`       | Run the `black` code formatter.                                                      |
| `pipenv run test`         | Run `pytest`.                                                                        |
| `pipenv run test-watch`   | Run `pytest` in watch mode.                                                          |
| `pipenv run cov`          | Run code coverage report.                                                            |
| `pipenv run cov-html`     | Generate a code coverage report in html format.                                      |
| `pipenv run build`        | Build python binary version                                                          |
| `pipenv run build-docker` | Run a local docker image                                                             |
